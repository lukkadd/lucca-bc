/** @<lextoparser.c>:: **/

#include <stdio.h>
#include <stdlib.h>
#include <lexer.h>
#include <parser.h>
#include <lextoparser.h>

extern FILE* source;
int lookahead;
char lexeme[MAXIDLEN + 1];

void match (int expected){
	if(lookahead == expected){
		lookahead = gettoken(source);
	}else{
		fprintf(stderr,"token mismatch... exiting");
		exit(-2);
	}
}
