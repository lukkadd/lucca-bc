/** @<lextoparser.h>:: **/

#pragma once
#define MAXIDLEN 63

/** FUNCTION: match
* INPUT: int (expected token)
* OUTPUT: void
* DESCRIPTION: match compares the token stored on lookahead
* against a provided expected token. If it matches it loads the next token
* otherwise it stops with an error **/
void match (int expected);
