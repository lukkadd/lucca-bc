/** @<parser.c> :: **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lexer.h>
#include <parser.h>
#include <lextoparser.h>
#include <virtmach.h>
#include <symtable.h>
#include <keywords.h>
#include <tokens.h>
#include <stack.h>

extern cell table[];
extern FILE* source;
extern float acc;
extern Stack *head;
extern int lookahead;
extern char lexeme[MAXIDLEN];

char symbol[MAXIDLEN];

/* mybc -> { [expr] cmdsep } cmdquit */
void mybc (void){
	lookahead = gettoken(source);

mybc_start:

cmd_start:
	// checks for exit command
	if(cmdquit()){
		exit(0);
	}
	//checks if lookahead contains the beggining of an expr
	if (is_FIRST_expr()) {
		expr();
		printf("%lg\n",acc);
	}
	//checks for a command separator
	if(cmdsep()){
		goto cmd_start;
	}else{
		printf("token mismatch... exiting");
		exit(-2);
	}
	//if the next lookahead is not an exit command, start the loop again
	if(!cmdquit()) goto mybc_start;
}

/* cmdsep -> \n | ; */
int cmdsep(){
	switch(lookahead){
		case '\n':
		case ';':
		match(lookahead);
		case EOF:
		return 1;
	}
	return 0;
}

/* cmdquit -> quit | ... */
int cmdquit(){
	switch(lookahead){
		case QUIT:
		case EXIT:
		case BYE:
		case LOGOUT:
			match(lookahead);
		case EOF:
		return 1;
	}
	return 0;
}

int is_FIRST_expr(){
	switch(lookahead){
		case '+':
		case '-':
		case '(':
		case UINT:
		case FLTP:
		case ID:
			return 1;
	}
	return 0;
}

void expr(void)
{
	//initializing flags
	int isnegate = 0;
	int oplus = 0;
	int otimes = 0;

	/* checking for the first '-', and setting the negate flag */
	if(lookahead == '-'){
		isnegate = 1;
		match(lookahead);
	}else if(lookahead == '+'){
		match(lookahead);
	}

T_begin:

F_begin:

	/*
	Abstracts the fact graph.
	At the end of this function
	a float value should be on acc.
	*/
	fact();

	//checking isnegate flag
	if(isnegate){
		exec_negate();
		isnegate = 0;
	}

	//checking otimes flag
	if (otimes) {
		basicops(otimes);
		otimes = 0;
	}

	//setting otimes flag based on lookahead
	if (lookahead == '*' || lookahead == '/') {
		otimes = lookahead;
		push(acc,&head);//acao semantica
		match(lookahead);
		goto F_begin;
	}

	// checking oplus flag
	if (oplus) {
		basicops(oplus);
		oplus = 0;
	}

	//setting oplus flag based on lookahead
	if (lookahead == '+' || lookahead == '-') {
		oplus = lookahead;
		push(acc,&head);//acao semantica
		match(lookahead);
		goto T_begin;
	}
}

/* ID | UINT | FLTP | (expr) */
void fact(){
	switch (lookahead) {
		case ID:
			strcpy(symbol,lexeme);
			match(ID);
			if(lookahead == '='){
				match('=');
				expr();
				// acao semantica 7
				updatesym(table,symbol,acc);
			}else{
				acc = retrievesym(table,symbol);
			}
			break;
		case UINT:
		case FLTP:
			acc = atof(lexeme); // acao semantica 4
			clearLexeme();
			match(lookahead);
			break;
		default:
			match('('); expr(); match(')');
	}
}
