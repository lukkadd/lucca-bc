/** @<parser.h>:: **/

#pragma once

/** FUNCTION: expr
* INPUT: void
* OUTPUT: void
* DESCRIPTION: abstracts the expr graph by reading
* the values of a series of lookaheads**/
void expr(void);

/** FUNCTION: fact
* INPUT: void
* OUTPUT: void
* ABSTRACTS: ID ['=' expr] | UINT | FLTP | '(' expr ')' **/
void fact(void);

/** FUNCTION: mybc
* INPUT: void
* OUTPUT: void
* ABSTRACTS: mybc -> {[expr] cmdsep} cmdquit**/
void mybc(void);

/** FUNCTION: cmdquit
* INPUT: int
* OUTPUT: void
* DESCRIPTION: returns 1 if lookahead is one of the following command tokens:
* QUIT | EXIT | BYE | LOGOUT | EOF 
* otherwise it returns 0
**/
int cmdquit(void);

/** FUNCTION: cmdsep
* INPUT: int
* OUTPUT: void
* DESCRIPTION: returns 1 if lookahead is one of the following command separators:
* ';' | '\n'
* otherwise it returns 0**/
int cmdsep(void);

/** FUNCTION: is_FIRST_expr
* INPUT: int
* OUTPUT: void
* DESCRIPTION: checks if the first token returned is a
* valid beggining for an expression **/
int is_FIRST_expr(void);
