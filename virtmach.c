/** @<virtmach.c> :: **/

#include <stdio.h>
#include <stdlib.h>
#include <stack.h>
#include <virtmach.h>

Stack *head = NULL;
float acc;

// realiza a operacao de negacao
void exec_negate(){
	acc = -acc;
};

// realiza a ação semantica
void basicops(char op) {
	switch(op) {
		case '*':
			acc = pop(&head) * acc;
			break;
		case '/':
			if (acc == 0){
				printf("Invalid operation\n");
				exit(-3);
			} else {
				acc = pop(&head) / acc;
			}
			break;
		case '+':
			acc = pop(&head) + acc;
			break;
		case '-':
			acc = pop(&head) - acc;
	}
}
