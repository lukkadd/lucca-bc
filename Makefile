mybc:	main.o lextoparser.o parser.o lexer.o keywords.o stack.o virtmach.o symtable.o
	gcc main.o lextoparser.o parser.o lexer.o keywords.o stack.o virtmach.o symtable.o -o mybc

lextoparser.o: lextoparser.c
	gcc -std=c99 -c lextoparser.c -I.

lexer.o: lexer.c
	gcc -std=c99 -c lexer.c -I.

parser.o: parser.c
	gcc -c parser.c -I.

main.o: main.c
	gcc -c main.c -I.

keywords.o: keywords.c
	gcc -std=c99 -c keywords.c -I.

stack.o: stack.c
	gcc -c stack.c -I.

virtmach.o: virtmach.c
	gcc -c virtmach.c -I.

symtable.o: symtable.c
	gcc -std=c99 -c symtable.c -I.

clean:
	rm *.o
