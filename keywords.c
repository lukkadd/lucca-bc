/** @<keywords.c> :: **/

#include <string.h>
#include <stdlib.h>
#include <keywords.h>

extern char *keyword[KEYWORD_ARR_SIZE];

char *keyword[] = {
	"quit", "exit", "bye", "logout"
};

int iskeyword(const char* symbol){
	for (int i = 0; i < KEYWORD_ARR_SIZE; ++i){
		if (!strcmp(symbol,keyword[i])){
			return i + QUIT;
		}
	}
	return 0;
}
