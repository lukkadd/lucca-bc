/** @<virtmach.h> :: **/

#pragma once

/** 
 * FUNCTION: basicops
 * INPUT: char
 * OUTPUT: void
 * DESCRIPTION: basicops receives a char with the
 * symbol of the operation that should be performed.
 * The result is stored in the accumulator (acc)
 **/
void basicops(char op);

/** 
 * FUNCTION: exec_negate
 * INPUT: void
 * OUTPUT: void
 * DESCRIPTION: This function takes the value
 * stored in the acc and negates it  
 **/
void exec_negate();